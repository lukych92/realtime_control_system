#ifndef OROCOS_MP_CONTROLLER_COMPONENT_HPP
#define OROCOS_MP_CONTROLLER_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <string>
#include <vector>
#include <ctime>

#include <std_msgs/Float64MultiArray.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Odometry.h>
#include <tf/tf.h>

class Mp_controller : public RTT::TaskContext{
public:
  Mp_controller(std::string const& name);
  bool configureHook();
  bool startHook();
  void updateHook();
  void stopHook();
  void cleanupHook();


  RTT::InputPort<nav_msgs::Odometry> _state;
  RTT::InputPort<std_msgs::Float64MultiArray> _reference_trajectory;
  RTT::OutputPort<std_msgs::Float64MultiArray > _control;

  RTT::OutputPort<nav_msgs::Path > _reference_trajectory_out;
  RTT::OutputPort<nav_msgs::Path > _actual_trajectory;



  std_msgs::Float64MultiArray controls;
  std::string refTrackFile;
  std::vector< std::vector< double > > reference_trajectory;
  std::vector< std::vector< double > > logs; //actual and reference position log
  std::vector< double > actual_pose; //actual and reference position log
  int numSteps; //number of controller steps
  int iter; //current iteration number

  nav_msgs::Path _reference_trajectory_msg;
  nav_msgs::Path _actual_trajectory_msg;
  geometry_msgs::PoseStamped _reference_pose_msg;
  geometry_msgs::PoseStamped _actual_pose_msg;


  bool readDataFromMessage( std_msgs::Float64MultiArray msg,
                         std::vector< std::vector< double > >& data
                            );

  bool readDataFromFile( const char* fileName,
                         std::vector< std::vector< double > >& data );

  void compute_controls(std::vector<double> ref_trajectory,
                        std::vector<double> actual_trajectory);


  void publish_reference_trajectory(void)
  {

      _reference_trajectory_msg.header.stamp = ros::Time::now();
      _reference_trajectory_msg.header.frame_id = "/odom";
      for(int i = 0;i < numSteps; ++i)
      {
        _reference_pose_msg.header.seq = i;
        _reference_pose_msg.pose.position.x = reference_trajectory[i][1];
        _reference_pose_msg.pose.position.y = reference_trajectory[i][2];
        _reference_pose_msg.pose.orientation = tf::createQuaternionMsgFromYaw(reference_trajectory[i][3]);

        _reference_trajectory_msg.poses.push_back(_reference_pose_msg);
      }

      _reference_trajectory_out.write(_reference_trajectory_msg);

  }

  void publish_actual_trajectory(void)
  {
      _actual_trajectory_msg.header.stamp = ros::Time::now();
      _actual_trajectory_msg.poses.push_back(_actual_pose_msg);
      _actual_trajectory.write(_actual_trajectory_msg);
  }

  void SendZeroControls()
  {
    controls.data[0] = 0.0;
    controls.data[1] = 0.0;
    _control.write(controls);
  }


  void printStep();
};
#endif
