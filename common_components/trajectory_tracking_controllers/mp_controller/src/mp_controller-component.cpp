#include "mp_controller-component.hpp"
#include <rtt/Component.hpp>
#include <rtt/os/Thread.hpp>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <iomanip>
#include <cstring>
#include <cstdlib>
#include <cmath>


#include "../include/mp_controller/mpc_common.h"
#include "../include/mp_controller/mpc_auxiliary_functions.h"

#define NX      ACADO_NX	/* number of differential states */
#define NU      ACADO_NU	/* number of control inputs */
#define N       ACADO_N     /* number of control intervals */
#define NY	    ACADO_NY	/* number of references, nodes 0..N - 1 */
#define NYN	    ACADO_NYN   /* Number of measurements/references on node N. */

real_t *aVy = MPC_getAcadoVariablesY();//;acado_getAcadoVariablesY();
real_t *aVyN = MPC_getAcadoVariablesYN();
real_t *aVx = MPC_getAcadoVariablesX();
real_t *aVx0 = MPC_getAcadoVariablesX0();
real_t *aVu = MPC_getAcadoVariablesU();

Mp_controller::Mp_controller(std::string const& name) :
    TaskContext(name),
    refTrackFile("reference_trajectory.txt")
{
  std::cout << "Model_predictive_controller constructed !" <<std::endl;

  this->ports()->addPort( "control_signal", _control )
    .doc("Output port for controls" );
  this->ports()->addPort( "reference_trajectory", _reference_trajectory )
    .doc("Output port for controls" );
  this->ports()->addEventPort( "state", _state )
    .doc("Input port for platform state" );

  this->ports()->addPort( "ref_traj", _reference_trajectory_out );
  this->ports()->addPort( "act_traj", _actual_trajectory );

  this->addAttribute( "reference_trajectory_file", refTrackFile );

  controls.data.resize(2);
  actual_pose.resize(3);
  RTT::os::Thread::setStackSize(8388608);

}

bool Mp_controller::configureHook()
{

    std::cout << "Mp_controller started !" <<std::endl;
    if(setActivity(new RTT::Activity(ORO_SCHED_RT,
                     10,
                     0,
                     1,
                     0,
                     getName())) == false){
      std::cout << "Unable to set activity!" << std::endl;
      return false;
    }
    // Set component activitiy
    std::cout << "Mp_controller configured !" <<std::endl;

  return true;
}

bool Mp_controller::startHook()
{

/*
  std_msgs::Float64MultiArray msg_new_trajectory;
  while(!(_reference_trajectory.readNewest(msg_new_trajectory) == RTT::NewData))
  {
//     std::cout << "I received trajectory!" << std::endl;
  }
*/
  MPC_resetSolverMem();
  iter=0;
//  if (!readDataFromMessage(msg_new_trajectory, reference_trajectory)){
  if (!readDataFromFile(refTrackFile.c_str(), reference_trajectory)){
      std::cout << "Cannot read refrence trajectory" << std::endl;
    return false;
  }
  else
  {
      std::cout << "Refrence trajectory read from message" << std::endl;
      numSteps=reference_trajectory.size();
      std::cout << iter << " " << numSteps << std::endl;
//      std::cout << reference_trajectory[0][0] << std::endl;
  }

  logs.resize(numSteps);
  for (int i = 0; i < logs.size(); ++i)
    logs[ i ].resize(9, 0.0); //x,y,phi actual and reference, controls

  printf("NX %d,NU %d,NY %d,NYN %d,N %d\n",NX,NU,NY,NYN,N);



  //Initialize solver
  if(MPC_initializeSolver() != 0){
    std::cout << "Solver initialisation failed!" << std::endl;
    return false;
  }


  for (int i = 0; i < N; ++i)
  {
      aVy[i * NY + 0] = reference_trajectory[ i ][ 1 ]; // x
      aVy[i * NY + 1] = reference_trajectory[ i ][ 2 ]; // y
      aVy[i * NY + 2] = reference_trajectory[ i ][ 3 ]; // theta
      aVy[i * NY + 3] = 0;//reference_trajectory[ i ][ 4 ]; // u1
      aVy[i * NY + 4] = 0;//reference_trajectory[ i ][ 5 ]; // u2
  }
  // Current state feedback
  for (int i = 0; i < NX; ++i)
      aVx0[ i ] = aVx[ i ];

//    printStep();
  actual_pose[0] = 0;
  actual_pose[1] = 0;
  actual_pose[2] = 0;

  _actual_trajectory_msg.header.frame_id = "/odom";
  _actual_pose_msg.header.seq = iter;
  _actual_pose_msg.pose.position.x = actual_pose[0];
  _actual_pose_msg.pose.position.y = actual_pose[1];
  _actual_pose_msg.pose.orientation = tf::createQuaternionMsgFromYaw(actual_pose[2]);

  publish_reference_trajectory();
  publish_actual_trajectory();

  // Warm-up the solver
  MPC_preparationStep();
  return true;
}

void Mp_controller::updateHook()
{
    if(iter >= numSteps)
    {
      std::cout << "We are done" << std::endl;
      this->stop(); //stop if we are done
      return;
    }

    nav_msgs::Odometry msg;
    if(_state.readNewest(msg) == RTT::NewData)
    {
        tf::Pose pose;
        tf::poseMsgToTF(msg.pose.pose, pose);
        double yaw_angle = tf::getYaw(pose.getRotation());
        double orientation = 0.0;

        if(yaw_angle >= 0) orientation = yaw_angle;
        else orientation = (2*M_PI)+yaw_angle;

        actual_pose[0] = msg.pose.pose.position.x;
        actual_pose[1] = msg.pose.pose.position.y;
        actual_pose[2] = orientation;
    }

  compute_controls(reference_trajectory[iter],actual_pose);
  _control.write(controls);


  logs[iter][0] = iter*getPeriod(); // time
  logs[iter][1] = actual_pose[0]; // actual x
  logs[iter][2] = actual_pose[1]; // actual y
  logs[iter][3] = actual_pose[2]; // actual theta
  logs[iter][4] = reference_trajectory[iter][1]; // reference x
  logs[iter][5] = reference_trajectory[iter][2]; // reference y
  logs[iter][6] = reference_trajectory[iter][3]; // reference z
  logs[iter][7] = controls.data[0];
  logs[iter][8] = controls.data[1];
  iter++;

  _actual_pose_msg.header.seq = iter;
  _actual_pose_msg.pose.position.x = actual_pose[0];
  _actual_pose_msg.pose.position.y = actual_pose[1];
  _actual_pose_msg.pose.orientation = tf::createQuaternionMsgFromYaw(actual_pose[2]);

  if(!(iter%100))
      publish_actual_trajectory();
}

void Mp_controller::stopHook() {
  std::cout << "Mp_controller executes stopping !" <<std::endl;
  // Save log to a file
  SendZeroControls();
  std::string file_name = "realtime_control_system_model_predictive_controller_logs.txt";
  std::ofstream dataLog( file_name.c_str() );
  if ( dataLog.is_open() ){
    dataLog << "# Model Predictive Controller log" << std::endl;
      time_t rawtime;
      struct tm * timeinfo;
      char buffer[80];

      time (&rawtime);
      timeinfo = localtime(&rawtime);

      strftime(buffer,sizeof(buffer),"%d-%m-%Y %H:%M:%S",timeinfo);
      std::string str(buffer);

    dataLog << "# " << str << std::endl;
    dataLog << "# Time horizon " << N*getPeriod() << " s" << std::endl;

    for (int i = 0; i < iter; i++){
      for (int j = 0; j < logs[ i ].size(); j++)
    dataLog << logs[ i ][ j ] << " ";
      dataLog << std::endl;
    }
    dataLog.close();
    std::cout << "Log file saved as " << file_name << std::endl;
  }
  else
    std::cout << "Log file could not be opened" << std::endl;
  reference_trajectory.clear();
}

void Mp_controller::cleanupHook() {
  std::cout << "Mp_controller cleaning up !" <<std::endl;
}

void Mp_controller::compute_controls(std::vector<double> ref_trajectory,
                      std::vector<double> actual_trajectory)
{
    aVx0[0] = actual_trajectory[0];
    aVx0[1] = actual_trajectory[1];
    aVx0[2] = actual_trajectory[2];


    int status;
    if((status = MPC_feedbackStep()) != 0){
      std::cout << "Iteration:" << iter << ", QP problem! QP status: "
            << status << std::endl;
      SendZeroControls();
      this->stop(); //solver error, stop execution
      return;
    }

    controls.data[0] = aVu[0];
    controls.data[1] = aVu[1];

    // Read new references
    for (int i = 0; i < N; ++i){
        if((iter + 1 + i) < numSteps){
            aVy[i * NY + 0] = reference_trajectory[ iter + 1 + i ][ 1 ]; // x
            aVy[i * NY + 1] = reference_trajectory[ iter + 1 + i ][ 2 ]; // y
            aVy[i * NY + 2] = reference_trajectory[ iter + 1 + i ][ 3 ]; // theta
        }
        else{
            aVy[i * NY + 0] = reference_trajectory[ numSteps - 1 ][ 1 ]; // x
            aVy[i * NY + 1] = reference_trajectory[ numSteps - 1 ][ 2 ]; // y
            aVy[i * NY + 2] = reference_trajectory[ numSteps - 1 ][ 3 ]; // theta
        }
        aVy[i * NY + 3] = 0; // u1
        aVy[i * NY + 4] = 0; // u2
    }
    MPC_preparationStep();
}

bool Mp_controller::readDataFromMessage( std_msgs::Float64MultiArray msg,
                       std::vector< std::vector< double > >& data
                          )
{
    int oneLineNumbers = 6;

    for(int i = 0; i<msg.data.size()/oneLineNumbers;++i)
    {
        std::vector< double > linedata;
        for(int j=0; j<oneLineNumbers;++j)
        {
            linedata.push_back(msg.data[oneLineNumbers*i+j]);
        }
//        for (std::vector<double>::iterator it = linedata.begin() ; it != linedata.end(); ++it)
//          std::cout << ' ' << *it;
//        std::cout << std::endl;
        data.push_back( linedata );

    }


  return true;
}



bool Mp_controller::readDataFromFile( const char* fileName, std::vector< std::vector< double > >& data )
{
  std::ifstream file( fileName );
  std::string line;

  if ( file.is_open() )
    {
      while( getline(file, line) )
    {
      std::istringstream linestream( line );
      std::vector< double > linedata;
      double number;

      while( linestream >> number )
        {
          linedata.push_back( number );
        }

      data.push_back( linedata );
    }

      file.close();
    }
  else
    return false;

  return true;
}

  void Mp_controller::printStep()
  {
int i=0;
      printf("Real-Time Iteration %d:  KKT Tolerance = %.3e\n", iter, MPC_getKKT() );
      printf("\tControls: ");
      for (i = 0; i < NU; ++i)
        printf("%.2f ",aVu[i]);
      printf("\n");
      printf("\tVar: ");
      for (i = 0; i < NX; ++i)
        printf("%.2f ",aVx[i]);
      printf("\n");
      printf("\tVar0: ");
      for (i = 0; i < NX; ++i)
        printf("%.2f ",aVx0[i]);
      printf("\n");
      printf("\tMessu: ");
      for (i = 0; i < NY; ++i)
        printf("%.2f ",aVy[i]);
      printf("\n");
      printf("\tRef: ");
      for (i = 0; i < NYN; ++i)
        printf("%.2f ",aVyN[i]);
      printf("\n");
  }


/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Mp_controller)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Mp_controller)

