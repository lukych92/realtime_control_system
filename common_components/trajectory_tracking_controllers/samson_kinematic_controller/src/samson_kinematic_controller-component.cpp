#include "samson_kinematic_controller-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>
#include <fstream>

Samson_kinematic_controller::Samson_kinematic_controller(std::string const& name) :
    TaskContext(name),
    refTrackFile("reference_trajectory.txt")
{
  std::cout << "Samson_kinematic_controller constructed !" <<std::endl;

  this->ports()->addPort( "control_signal", _control )
    .doc("Output port for controls" );
  this->ports()->addEventPort( "state", _state )
    .doc("Input port for platform state" );

  this->ports()->addPort( "ref_traj", _reference_trajectory_out );
  this->ports()->addPort( "act_traj", _actual_trajectory );


  this->addAttribute( "reference_trajectory_file", refTrackFile );

  controls.data.resize(2);
  actual_pose.resize(3);
}

bool Samson_kinematic_controller::configureHook(){
  std::cout << "Samson_kinematic_controller configured !" <<std::endl;

  return true;
}

bool Samson_kinematic_controller::startHook(){
  std::cout << "Samson_kinematic_controller started !" <<std::endl;
  iter = 0;
  if (!readDataFromFile(refTrackFile.c_str(), reference_trajectory)){
    std::cout << "Cannot read refrence trajectory" << std::endl;
    return false;
  }
  else
  {
      std::cout << "Refrence trajectory read from " << refTrackFile.c_str() << std::endl;
      numSteps=reference_trajectory.size();
      std::cout << iter << " " << numSteps << std::endl;
//      std::cout << reference_trajectory[0][0] << std::endl;
  }

  logs.resize(numSteps);
  for (int i = 0; i < logs.size(); ++i)
    logs[ i ].resize(9, 0.0); //x,y,phi actual and reference, controls

  actual_pose[0] = 0;
  actual_pose[1] = 0;
  actual_pose[2] = 0;

  _actual_trajectory_msg.header.frame_id = "/odom";
  _actual_pose_msg.header.seq = iter;
  _actual_pose_msg.pose.position.x = actual_pose[0];
  _actual_pose_msg.pose.position.y = actual_pose[1];
//  _actual_pose_msg.pose.orientation = tf::createQuaternionMsgFromYaw(actual_pose[2]);

  publish_reference_trajectory();
  publish_actual_trajectory();

  return true;
}

void Samson_kinematic_controller::updateHook()
{
    if(iter >= numSteps)
    {
      std::cout << "We are done" << std::endl;
      this->stop(); //stop if we are done
      return;
    }

    nav_msgs::Odometry msg;
    if(_state.readNewest(msg) == RTT::NewData)
    {
        tf::Pose pose;
        tf::poseMsgToTF(msg.pose.pose, pose);
        double yaw_angle = tf::getYaw(pose.getRotation());
        double orientation = 0.0;

        if(yaw_angle >= 0) orientation = yaw_angle;
        else orientation = (2*M_PI)+yaw_angle;

        actual_pose[0] = msg.pose.pose.position.x;
        actual_pose[1] = msg.pose.pose.position.y;
        actual_pose[2] = orientation;
    }

    compute_controls(reference_trajectory[iter],actual_pose);
    _control.write(controls);


    logs[iter][0] = iter*getPeriod(); // time
    logs[iter][1] = actual_pose[0]; // actual x
    logs[iter][2] = actual_pose[1]; // actual y
    logs[iter][3] = actual_pose[2]; // actual theta
    logs[iter][4] = reference_trajectory[iter][1]; // reference x
    logs[iter][5] = reference_trajectory[iter][2]; // reference y
    logs[iter][6] = reference_trajectory[iter][3]; // reference z
    logs[iter][7] = controls.data[0];
    logs[iter][8] = controls.data[1];
    iter++;

    _actual_pose_msg.header.seq = iter;
    _actual_pose_msg.pose.position.x = actual_pose[0];
    _actual_pose_msg.pose.position.y = actual_pose[1];
//    _actual_pose_msg.pose.orientation = tf::createQuaternionMsgFromYaw(actual_pose[2]);

    if(!(iter%100))
        publish_actual_trajectory();
}

void Samson_kinematic_controller::stopHook() {
  std::cout << "Samson_kinematic_controller executes stopping !" <<std::endl;
  // Save log to a file
  SendZeroControls();
  std::string file_name = "realtime_control_system_samson_kinematic_controller_logs.txt";
  std::ofstream dataLog( file_name.c_str() );
  if ( dataLog.is_open() ){
    dataLog << "# Samson Kinematic Controller log" << std::endl;
      time_t rawtime;
      struct tm * timeinfo;
      char buffer[80];

      time (&rawtime);
      timeinfo = localtime(&rawtime);

      strftime(buffer,sizeof(buffer),"%d-%m-%Y %H:%M:%S",timeinfo);
      std::string str(buffer);

    dataLog << "# " << str << std::endl;

    for (int i = 0; i < iter; i++){
      for (int j = 0; j < logs[ i ].size(); j++)
    dataLog << logs[ i ][ j ] << " ";
      dataLog << std::endl;
    }
    dataLog.close();
    std::cout << "Log file saved as " << file_name << std::endl;
  }
  else
    std::cout << "Log file could not be opened" << std::endl;
  reference_trajectory.clear();
}

void Samson_kinematic_controller::cleanupHook() {
  std::cout << "Samson_kinematic_controller cleaning up !" <<std::endl;
}

void Samson_kinematic_controller::compute_controls(std::vector<double> reference_trajectory,
                                                   std::vector<double> actual_trajectory)
{
    double K1 = 1.0;
    double K2 = 1.0;

    // cos(theta)(q_1-q_1d)+sin(theta)(q_2-q_2d)
    double e_x = cos(actual_trajectory[2])*(actual_trajectory[0]-reference_trajectory[1])
            + sin(actual_trajectory[2])*(actual_trajectory[1]-reference_trajectory[2]);
    // -sin(theta)(q_1-q_1d)+cos(theta)(q_2-q_2d)
    double e_y = -sin(actual_trajectory[2])*(actual_trajectory[0]-reference_trajectory[1])
            + cos(actual_trajectory[2])*(actual_trajectory[1]-reference_trajectory[2]);

    double e_theta = actual_trajectory[2]-reference_trajectory[3];

    controls.data[0] = reference_trajectory[4]*cos(e_theta)-K1*e_x;
    controls.data[1] = -K2*e_theta+reference_trajectory[5]-e_y*reference_trajectory[4]*sin(e_theta)/e_theta;
}

bool Samson_kinematic_controller::readDataFromFile( const char* fileName, std::vector< std::vector< double > >& data )
{
  std::ifstream file( fileName );
  std::string line;

  if ( file.is_open() )
    {
      while( getline(file, line) )
    {
      std::istringstream linestream( line );
      std::vector< double > linedata;
      double number;

      while( linestream >> number )
        {
          linedata.push_back( number );
        }

      data.push_back( linedata );
    }

      file.close();
    }
  else
    return false;

  return true;
}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Samson_kinematic_controller)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Samson_kinematic_controller)
