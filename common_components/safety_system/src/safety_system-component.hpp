#ifndef OROCOS_SAFETY_SYSTEM_COMPONENT_HPP
#define OROCOS_SAFETY_SYSTEM_COMPONENT_HPP

#include <rtt/RTT.hpp>

#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Bool.h>

#include <sensor_msgs/LaserScan.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>

class Safety_system : public RTT::TaskContext{
 public:
  Safety_system(std::string const& name);
  bool configureHook();
  bool startHook();
  void updateHook();
  void stopHook();
  void cleanupHook();

  RTT::InputPort<sensor_msgs::LaserScan> _scan;
  RTT::InputPort<nav_msgs::Odometry > _pose;
  RTT::OutputPort<std_msgs::Bool> _enable;

private:
  bool DistTooSmall;
  bool LaserTimeoutExpired;
  bool OutOfBounds;
  bool MCTimeoutExpired;
  bool Enabled;
  static const double MinSafeDist;
  static const double Period; 
  static const double LaserTimeoutTime;
  static const double MCTimeoutTime;
  int LaserTimeoutCntr; //timeout counter for Laser
  int MCTimeoutCntr; //timeout counter for motion capture

  double XLowerBound;
  double XUpperBound;
  double YLowerBound;
  double YUpperBound;

  void CheckDistance();
  void CheckPose();
  void PrintErrorMessage();

  bool first_print;
};
#endif
