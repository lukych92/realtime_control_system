#include "safety_system-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>

const double Safety_system::Period=0.005; // [s]
const double Safety_system::LaserTimeoutTime=4; // [s]
const double Safety_system::MCTimeoutTime=0.5; // [s]
const double Safety_system::MinSafeDist=0.5; //!!! [m]

Safety_system::Safety_system(std::string const& name) : 
  TaskContext(name), XLowerBound(-2), XUpperBound(2), YLowerBound(-2),
  YUpperBound(2)
						      
{
  this->ports()->addPort("scan",_scan).doc("Input from laser scanner");
  this->ports()->addPort("pose",_pose).doc("Input from motion capture");
  this->ports()->addPort("enable",_enable).doc("Output for enable signal");

  this->addProperty( "XLowerBound", XLowerBound ).doc("X lower bound");
  this->addProperty( "XUpperBound", XUpperBound ).doc("X upper bound");
  this->addProperty( "YLowerBound", YLowerBound ).doc("Y lower bound");
  this->addProperty( "YUpperBound", YUpperBound ).doc("Y upper bound");
  std::cout << "Safety_system constructed !" <<std::endl;
}

bool Safety_system::configureHook(){
  std::cout << "Safety_system configured !" <<std::endl;
  return this->setPeriod(Period);
}

bool Safety_system::startHook(){
  DistTooSmall = false; //!!!!!!
  OutOfBounds = false;
  LaserTimeoutExpired = false;
  MCTimeoutExpired = false;
  Enabled = false;
  LaserTimeoutCntr = 0;
  MCTimeoutCntr = 0;
  first_print = false;
  std::cout << "Safety_system started !" <<std::endl;
  return true;
}

void Safety_system::updateHook()
{
  //std::cout << "Safety_system executes updateHook !" <<std::endl;
  std_msgs::Bool enable;

  CheckDistance();
  CheckPose();
//  PrintErrorMessage();
  if(!DistTooSmall && !LaserTimeoutExpired && !OutOfBounds 
     && !MCTimeoutExpired){ //everything ok
    enable.data = true;
    if(!Enabled){
      Enabled = true;
      std::cout << "Enable positive" << std::endl;
      first_print = true;
      _enable.write(enable);
    }
  }
  else{
    enable.data = false;
    if(Enabled){
      Enabled = false;
	  if(first_print)
	  {
          std::cout << "Enable negative" << std::endl;
          PrintErrorMessage();
          first_print = false;
      }
      _enable.write(enable);
    }
  }
}

void Safety_system::stopHook()
{
  std_msgs::Bool enable;
  enable.data = true;
  _enable.write(enable);
  std::cout << "Safety_system executes stopping !" <<std::endl;
}

void Safety_system::cleanupHook() {
  std::cout << "Safety_system cleaning up !" <<std::endl;
}

void Safety_system::CheckDistance()
{
  sensor_msgs::LaserScan scan;
  if(_scan.readNewest(scan) == RTT::NewData){
    LaserTimeoutCntr = 0;
    LaserTimeoutExpired = false;
    DistTooSmall = false;
    for (unsigned int i=0; i < scan.ranges.size(); ++i){
      if (scan.ranges[i] < scan.range_min)
	continue;
      if (scan.ranges[i] < MinSafeDist){
	//std::cout << "Distance too small: " << scan.ranges[i] << " "
	//	  << i << std::endl;
	//DistTooSmall = true;
  DistTooSmall = false;	
  break;
      }
    }
  }
  //Check if laser timeout expired
  if(LaserTimeoutCntr >= LaserTimeoutTime/Period){
    //std::cout << "Laser time expired!" << std::cout;
    LaserTimeoutExpired = true;
  }
  else
    LaserTimeoutCntr++;
}

void Safety_system::CheckPose()
{
  nav_msgs::Odometry pose;
  if(_pose.readNewest(pose) == RTT::NewData){
    MCTimeoutCntr = 0;
    MCTimeoutExpired = false;
    OutOfBounds = (pose.pose.pose.position.x < XLowerBound) || (pose.pose.pose.position.x > XUpperBound) ||
      (pose.pose.pose.position.y < YLowerBound) || (pose.pose.pose.position.y > YUpperBound);
  }

  //Check if motion capture timeout expired
  if(MCTimeoutCntr >= MCTimeoutTime/Period){
    //std::cout << "Motion capture time expired!" << std::cout;
    MCTimeoutExpired = true;
  }
  else
    MCTimeoutCntr++;
}

void Safety_system::PrintErrorMessage()
{
  if(DistTooSmall)
    std::cout << "Distance to obstacle too small." << std::endl;
  if(LaserTimeoutExpired)
    std::cout << "No new messages from laser scanner. Timeout expired." 
	      << std::endl;
  if(OutOfBounds)
    std::cout << "Platfrom position out of bounds." << std::endl;
  if(MCTimeoutExpired)
    std::cout << "No new messages from motion capture. Timeout expired." 
	      << std::endl;
}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Safety_system)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Safety_system)
