#ifndef OROCOS_CONTROLL_SYSTEM_ARBITER_COMPONENT_HPP
#define OROCOS_CONTROLL_SYSTEM_ARBITER_COMPONENT_HPP

#include <rtt/RTT.hpp>

#include <geometry_msgs/Twist.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Float64MultiArray.h>


class Controll_system_arbiter : public RTT::TaskContext{

	/*
	 * Komponent realizuje zadanie Arbitra sygnalow
	 *
	 * wejscia:
	 * DaneZJoysticka - Odczyty z joysticka; geometry_msgs::Twist;
	 * msg.linear.x – wychylenie przód; msg.linear.z – wychylenie bok
	 *
	 *
	 * Controls - zadane momenty na silniki; vector <double>; vector <double>
	 *
	 * 	Enable - sygnaly enable; int;
	 * 	"0 → platforma zatrzymana 1-->platforma rusza się"
	 *
	 * wyjscia:
	 *
	 * OstateczneSterowaniaMomenty;	Wartości sterowań po arbitrażu;	vector<double>;
	 * OstateczneSterowaniaMomenty[0] przednie, lewe. OstateczneSterowaniaMomenty[1] przednie prawe,
	 * OstateczneSterowaniaMomenty[2] lewe tylne, OstateczneSterowaniaMomenty[3] prawe tylne
	 *
	 * TrybDziałana; Określa czy robot działa automatycznie czy na joystcku; int
	 * "0 → sterowanie automatyczne, 1-->sterowanie reczne"
     * 0 -> platform stop
     * 1 -> teleoperation
     * 2 -> controller control
	 *
	 */
public:
	Controll_system_arbiter(std::string const& name);

 //   std_msgs::Int64 mode;

	bool configureHook();
	bool startHook();
	void updateHook();
	void stopHook();
	void cleanupHook();

    void zatrzymanie_platformy(std_msgs::Float64MultiArray&);
    void zadanie_wartosci_joystick(std_msgs::Float64MultiArray&, geometry_msgs::Twist );

	/******************	PORTS DECLARATIONS	****************/

	/**			INPUT PORTS			**/
	// declaration of port transferring the state of desired ports
	// cos od joysticka			RTT::InputPort <std::vector<int> > DIOInput;

    RTT::InputPort<geometry_msgs::Twist> _cmd_vel;

	// port wejsciowy do przesylania sterowan od sterownika

    RTT::InputPort <std_msgs::Float64MultiArray> _controls_controller;

	//port wejsciowy do przekazywania informacji na temat
	//trybu dzialania systemu - sterowanie reczne czy automatyczne
	//od Systemu Bezpieczenstwa dostaje informacje, czy zostal wcisniety
	//przycisk bezpieczenstwa czy pojawila sie przeszkoda

    RTT::InputPort <std_msgs::Bool> _enable;

	/**			OUTPUT PORTS			**/

	//port wyjsciowy przesylajacy ostateczna wersje sterowan
    RTT::OutputPort <geometry_msgs::Twist> _controls;
//	RTT::OutputPort <std::vector<double> > OstateczneSterowaniaMomenty;

	//port wyjsciowy do przesylania informacji o trybie dzialania
//	RTT::OutputPort <int> TrybDzialania;
//    RTT::OutputPort <std_msgs::Int64> _mode;
	//RTT::OutputPort <std_msgs::Int16> TrybDzialania;

    bool system_enable;

};
#endif
