#include "controll_system_arbiter-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>
#include <cmath>

//
Controll_system_arbiter::Controll_system_arbiter(std::string const& name) :
TaskContext(name)
{
 //   mode.data = 0;
	std::cout << "Controll_system_arbiter constructed !" <<std::endl;

	/*
	 * 			INPUT PORTS
	 */
    this->ports()->addEventPort("cmd_vel",_cmd_vel).doc("Receiving a message here will wake up this component.");
    this->ports()->addEventPort("controls_controller",_controls_controller).doc("Receiving a message here will wake up this component.");
    this->ports()->addEventPort("enable",_enable).doc("Receiving a message here will wake up this component.");

	/*
	 * 			OUTPUT PORTS
	 */

    this->ports()->addPort("controls", _controls).doc("controls");
 //   this->ports()->addPort("mode", _mode).doc("mode");
}

bool Controll_system_arbiter::configureHook(){
	std::cout << "Controll_system_arbiter conf started !" <<std::endl;
	return true;

}

bool Controll_system_arbiter::startHook(){
	std::cout << "Controll_system_arbiter started !" <<std::endl;
	return true;
}

void Controll_system_arbiter::updateHook()
{
    std_msgs::Bool enable; // wartosc enable, domyslnie ustawiona na zatrzymanie platformy
    geometry_msgs::Twist msg_cmd_vel; // sterowania wychodzace z arbitra;

    if(_enable.readNewest(enable) == RTT::NewData)
    {
        system_enable = enable.data;
    }

    if(system_enable)
    {
        geometry_msgs::Twist msg_teleop; // dane potrzebne dla joysticka
        if(_cmd_vel.readNewest(msg_teleop) == RTT::NewData)
        {
            msg_cmd_vel.linear.x = msg_teleop.linear.x;
            msg_cmd_vel.angular.z = msg_teleop.angular.z;
        }

        std_msgs::Float64MultiArray msg_controller; // dane sygnalow od sterownika
        if(_controls_controller.readNewest(msg_controller) == RTT::NewData)
        {
            msg_cmd_vel.linear.x=msg_controller.data[0];
            msg_cmd_vel.angular.z=msg_controller.data[1];
        }
    }
    else
    {
        msg_cmd_vel.linear.x=0;
        msg_cmd_vel.angular.z=0;
    }



    _controls.write(msg_cmd_vel);


}

void Controll_system_arbiter::stopHook() {
	std::cout << "Controll_system_arbiter executes stopping !" <<std::endl;
}

void Controll_system_arbiter::cleanupHook() {
	std::cout << "Controll_system_arbiter cleaning up !" <<std::endl;
}

void Controll_system_arbiter::zatrzymanie_platformy(std_msgs::Float64MultiArray &sterowania)
{
    sterowania.data[0] = 0;
    sterowania.data[1] = 0;
}

void Controll_system_arbiter::zadanie_wartosci_joystick(std_msgs::Float64MultiArray &sterowania, geometry_msgs::Twist msg){

/*	double wspol_liniowy=5;
	double wspol_katowy=6;
	double lin, ang, r, eta1, eta2;

	msg.angular.z=msg.angular.z*-1;
	r=sqrt(msg.linear.x*msg.linear.x + msg.angular.z*msg.angular.z);
	//		std::cout << "Jezdem " << r << "abs  "  <<std::abs(r) <<std::endl;

	if (r > 0.01)              {
		//      std::cout << "Jezdem " << std::endl;
		lin = std::max(std::abs(msg.linear.x), std::abs(msg.angular.z))*msg.linear.x/r; //sinus
		ang = std::max(std::abs(msg.linear.x), std::abs(msg.angular.z))*msg.angular.z/r; //kosinus
	}

	else{
		lin=0;
		ang=0;
	}

//	RTT::log(RTT::Info) << "linear:  " << lin << RTT::endlog();
//	RTT::log(RTT::Info) << "ang:  " << ang << RTT::endlog();
	//  linear_vector = RexMoCo_U.XJoy*4000; //max 16 min 0
	//	  radial_vector = RexMoCo_U.YJoy*4000;
	//
	//	RexMoCo_Y.eta1 =8192 -linear_vector + 1*radial_vector;
	//
	//	  RexMoCo_Y.eta2 =8192 + linear_vector + 1*radial_vector ;

	eta1=1.5*(lin*wspol_liniowy-ang*wspol_katowy);
	eta2=1.5*(lin*wspol_liniowy+ang*wspol_katowy);
*/

    double v = msg.linear.x;
    double w = msg.angular.z;

    sterowania.data[0] = msg.linear.x;
    sterowania.data[1] = msg.angular.z;

//	std::cout << "\n *************************** \n Wartosci: \n eta1: " << eta1 << " \n eta2: " << eta2 << std::endl;
}



/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Controll_system_arbiter)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Controll_system_arbiter)
