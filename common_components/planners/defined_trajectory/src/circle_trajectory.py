#!/usr/bin/env python

# /*****************************************************************************
# *                                                                           *
# *   circle_trajectory.py                                                    *
# *                                                                           *
# *   ROS                                                                     *
# *                                                                           *
# *   Copyright (C) 2017 by Lukasz Chojnacki                                  *
# *   lukasz.chojnacki@pwr.edu.pl                                             *
# *                                                                           *
# *   This program is free software; you can redistribute it and/or modify    *
# *   it under the terms of the GNU General Public License as published by    *
# *   the Free Software Foundation; either version 2 of the License, or       *
# *   (at your option) any later version.                                     *
# *                                                                           *
# *   This program is distributed in the hope that it will be useful,         *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
# *   GNU General Public License for more details.                            *
# *                                                                           *
# *   You should have received a copy of the GNU General Public License       *
# *   along with this program; if not, write to the                           *
# *   Free Software Foundation, Inc.,                                         *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
# *                                                                           *
# *****************************************************************************/
import rospy
import tf
import numpy as np
import matplotlib.pyplot as plt


from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path
from std_msgs.msg import Bool, Float64MultiArray


def DesiredPlan(data):
    global ackFlag
    global pubTrajrviz
    global pubTrajctrl

    # rospy.loginfo("Hi!")
    # rospy.loginfo(data.header)


    x = data.pose.position.x
    y = data.pose.position.y

    quaternion = (
        data.pose.orientation.x,
        data.pose.orientation.y,
        data.pose.orientation.z,
        data.pose.orientation.w)

    euler = tf.transformations.euler_from_quaternion(quaternion)

    theta = 0.0
    if euler[2] >= 0:
        theta = euler[2]
    else:
        theta = (2*np.pi)+euler[2]


    robot_state = [x,y,theta]
    rospy.loginfo("Robot is moving to "+str(robot_state))


    ### Build circle trajectory
    ## parameter
    T = 8 * 3.14
    R = 1
    w = 2 * 3.14 / T

    ## circle trajectory generator
    time = []
    reference_x = []
    reference_y = []
    reference_orientation = []
    velocity_linear = []
    velocity_angular = []

    for t in np.linspace(0, T, 314 * 4):
        time.append(t)
        orientation = (w * t) - np.pi / 2
        reference_x.append(R * np.cos(orientation))
        reference_y.append(R * np.sin(orientation) + R)
        reference_orientation.append(orientation + np.pi / 2)
        velocity_linear.append(0.3)
        velocity_angular.append(0.3)

    ## publishing
    path = Path()
    path.header.stamp = rospy.get_rostime()
    path.header.frame_id = "/odom"

    for i in range(len(time)):
        tmp_pose = PoseStamped()
        tmp_pose.header.seq = i
        tmp_pose.pose.position.x = reference_x[i]
        tmp_pose.pose.position.y = reference_y[i]

        quaternion = tf.transformations.quaternion_from_euler(0.0,
                                                              0.0,
                                                              reference_orientation[i]
                                                              )

        tmp_pose.pose.orientation.x = quaternion[0]
        tmp_pose.pose.orientation.y = quaternion[1]
        tmp_pose.pose.orientation.z = quaternion[2]
        tmp_pose.pose.orientation.w = quaternion[3]

        path.poses.append(tmp_pose)


    reference_trajectory = Float64MultiArray()
    for i in range(len(time)):
        reference_trajectory.data.append(time[i])
        reference_trajectory.data.append(reference_x[i])
        reference_trajectory.data.append(reference_y[i])
        reference_trajectory.data.append(reference_orientation[i])
        reference_trajectory.data.append(velocity_linear[i])
        reference_trajectory.data.append(velocity_angular[i])


    pubTrajrviz.publish(path)
    pubTrajctrl.publish(reference_trajectory)

    # if ackFlag == True:
    #
        # m = 0
    #
    #     # read data from topic /desired_plan
    #     # build a point map form a recived vector
    #     Yd = PointListFromVector(data.desired_plan)
    #     print
    #     "Yd=", Yd
    #
    #     ackFlag = not ackFlag
    #
    #     # generate trajectory
    #     q1, q2, q3, trajParts = LiftedNewtonAlgorithm(Yd)
    #
    #     trajmsg = traj()
    #
    #     # fill messafe
    #     # trajmsg.q1 = q1[m]
    #     # trajmsg.q2 = q2[m]
    #     # trajmsg.q3 = q3[m]
    #     trajmsg.q1 = q1
    #     trajmsg.q2 = q2
    #     trajmsg.q3 = q3
    #     trajmsg.step = step
    #     trajmsg.notify = notify
    #
    #     # publish message at topic /traj
    #     pubTraj.publish(trajmsg)
    #     trajParts = trajParts - 1
    #     m = m + 1

    # else:
    #     rospy.loginfo("Sorry, I'm working. ack flag is false. ")


def Ready(data):
    # global ackFlag
    # global trajParts, m

    # read data from topic /ready
    rospy.loginfo("Trajectory was realized")
    readyFlag = data.ready

    #    print readyFlag,trajParts,m


    # if readyFlag > 0 and trajParts > 0:
    #     readyFlag = False
        # create message
        # trajmsg = traj()

        # fill message
        # trajmsg.q1 = q1
        # trajmsg.q2 = q2
        # trajmsg.q3 = q3
        # trajmsg.step = step
        # trajmsg.notify = notify

        # publish message at topic /traj
        # pubTraj.publish(trajmsg)
        # trajParts = trajParts - 1
        # m = m + 1

    # if readyFlag > 0 or not trajParts:
    #     ackFlag = True
    # else:
    #     ackFlag = False

    # create message of type ack
    ackmsg = ack()

    # fill message
    ackmsg.ack = ackFlag

    # publish message at topic /ack
    pubAck.publish(ackmsg)

    # print(readyFlag,trajParts)


def node():
    global pubAck
    global pubTrajrviz
    global pubTrajctrl

    # Init ROS Node /LiftedNewtonAlgorithm
    rospy.init_node('circle_reference_trajectory')

    # Publishers
    pubAck = rospy.Publisher("ack", Bool, queue_size=100)
    pubTrajrviz = rospy.Publisher("trajectory_rviz", Path, queue_size=100)
    pubTrajctrl = rospy.Publisher("trajectory_controller", Float64MultiArray, queue_size=100)

    # Subscribers
    rospy.Subscriber("move_base_simple/goal", PoseStamped, DesiredPlan)
    rospy.Subscriber("ready", Bool, Ready)

    rospy.spin()


# def PointListFromVector(vector):
#     Yd = []
#
#     for m in range(0, len(vector) - system._dim['n'], 2):  # zmienic zalezne od modelu
#         yi = []
#         for n in range(2):
#             yi.append(vector[m + n])
#         Yd.append(yi)
#     yi = []
#
#     for m in range(len(vector) - system._dim['n'], len(vector)):
#         yi.append(vector[m])
#     Yd.append(yi)
#     return Yd


if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass





