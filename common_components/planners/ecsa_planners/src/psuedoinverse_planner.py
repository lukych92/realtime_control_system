#!/usr/bin/env python

# /*****************************************************************************
# *                                                                           *
# *   path_planner_pseudoinverse.py                                             *
# *                                                                           *
# *   ROS                                                                     *
# *                                                                           *
# *   Copyright (C) 2016 by Lukasz Chojnacki                                  *
# *   lukasz.chojnacki@pwr.edu.pl                                               *
# *                                                                           *
# *   This program is free software; you can redistribute it and/or modify    *
# *   it under the terms of the GNU General Public License as published by    *
# *   the Free Software Foundation; either version 2 of the License, or       *
# *   (at your option) any later version.                                     *
# *                                                                           *
# *   This program is distributed in the hope that it will be useful,         *
# *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
# *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
# *   GNU General Public License for more details.                            *
# *                                                                           *
# *   You should have received a copy of the GNU General Public License       *
# *   along with this program; if not, write to the                           *
# *   Free Software Foundation, Inc.,                                         *
# *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
# *                                                                           *
# *****************************************************************************/

import os, sys
sys.path.append(os.path.join(os.path.expanduser('~'), 'casadi-py27-np1.9.1-v3.0.0'))
from casadi import *

from ECSA.Control import Fourier
from ECSA.System_base import Monocycle
from ECSA.Algorithm import Pseudoinverse

import rospy
import tf
import numpy as np


from geometry_msgs.msg import PoseArray
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Path
from std_msgs.msg import Bool, Float64MultiArray

from dynamic_reconfigure.server import Server
from ecsa_planners.cfg import PseudoinverseConfig

ackFlag = True

# -------------------------------------------#
# Algorithm parameters

# derivative time
T = int(rospy.get_param('/ecsa_planner/algorithm/T', 10))  # s
rospy.set_param('/ecsa_planner/algorithm/T', T)
# T = T * 1000#ms

# algoritm step
gamma = float(rospy.get_param('/ecsa_planner/algorithm/gamma', 0.94))
rospy.set_param('/ecsa_planner/algorithm/gamma', gamma)

# total tolerance error
TOL_tmp = rospy.get_param('/ecsa_planner/algorithm/TOL', [1, 10, -1])
TOL = 10 ** (-3)  # float(TOL_tmp[0])*pow(float(TOL_tmp[1]),float(TOL_tmp[2]))
rospy.set_param('/ecsa_planner/algorithm/TOL', TOL)

# maximum number of steps
k_max = int(rospy.get_param('/ecsa_planner/algorithm/k_max', 200))
rospy.set_param('/ecsa_planner/algorithm/k_max', k_max)

#
dts = rospy.get_param('/ecsa_planner/controller/dts', 0.01)  # s
rospy.set_param('/ecsa_planner/controller/dts', dts)

#
# Ts = rospy.get_param('/robrex/controller/Ts', 20)  # s
# rospy.set_param('/robrex/controller/Ts', Ts)
#
# d_notify = int(rospy.get_param('/robrex/controller/d_notify', 1))
# rospy.set_param('/robrex/controller/d_notify', d_notify)
#
# step = Ts / dts

# notify = step - d_notify

# -------------------------------------------#
opts = {}
opts['T'] = T
opts['gamma0'] = gamma
opts['TOL'] = TOL
opts['k_max'] = k_max
opts['dts'] = dts

la1 = [0.5, 0.0, 0.0, 0.0, 0.0]
la2 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
opts['lambda0'] = vertcat(la1, la2)
opts['lambda'] = [[len(la1), 0], [len(la2), 0]]

opts['viz'] = [False, False, 0.2]

fourier = Fourier(opts)
system = Monocycle(fourier)


def Algorithm(Yd):
    global robot_position
    ## translate point list

    opts['q0'] = [0,0,0]
    opts['x0'] = []
    opts['yd'] = Yd


    # create algorithm object
    algorithm = Pseudoinverse(system, opts)

    # run algorithm
    rospy.loginfo("Robot is moving from "+str(opts['q0'])+" to "+str(opts['yd']))
    out = algorithm.run()
    rospy.loginfo("Planning finished")

    return out['tgrid'], out['q'][0, :], out['q'][1, :], out['q'][2, :], out['u'][0, :], out['u'][1, :]


def DesiredPlan(data):
    global ackFlag
    global pubTrajrviz
    global pubTrajctrl
    global pubPoses
    global pubInitPose

    # rospy.loginfo("Hi!")
    # rospy.loginfo(data.header)


    x = data.pose.position.x
    y = data.pose.position.y

    quaternion = (
        data.pose.orientation.x,
        data.pose.orientation.y,
        data.pose.orientation.z,
        data.pose.orientation.w)

    euler = tf.transformations.euler_from_quaternion(quaternion)

    theta = 0.0
    if euler[2] >= 0:
        theta = euler[2]
    else:
        theta = (2*np.pi)+euler[2]


    robot_state = [x,y,theta]

    # generate trajectory
    time, reference_x, reference_y, reference_orientation, velocity_linear, velocity_angular = Algorithm(robot_state)

    ## publishing
    path = Path()
    path.header.stamp = rospy.get_rostime()
    path.header.frame_id = "odom"

    pose_array = PoseArray()
    pose_array.header.stamp = rospy.get_rostime()
    pose_array.header.frame_id = 'odom'



    for i in range(len(time)):
        tmp_pose_stamped = PoseStamped()
        tmp_pose_stamped.header.seq = i
        tmp_pose_stamped.header.frame_id = 'odom'

        tmp_pose = Pose()
        tmp_pose.position.x = reference_x[i]
        tmp_pose.position.y = reference_y[i]

        quaternion = tf.transformations.quaternion_from_euler(0.0,
                                                              0.0,
                                                              reference_orientation[i]
                                                              )

        tmp_pose.orientation.x = quaternion[0]
        tmp_pose.orientation.y = quaternion[1]
        tmp_pose.orientation.z = quaternion[2]
        tmp_pose.orientation.w = quaternion[3]


        tmp_pose_stamped.pose = tmp_pose

        path.poses.append(tmp_pose_stamped)

        # if not i%10:
        pose_array.poses.append(tmp_pose)


    reference_trajectory = Float64MultiArray()
    for i in range(len(time)):
        reference_trajectory.data.append(time[i])
        reference_trajectory.data.append(reference_x[i])
        reference_trajectory.data.append(reference_y[i])
        reference_trajectory.data.append(reference_orientation[i])
        reference_trajectory.data.append(velocity_linear[i])
        reference_trajectory.data.append(velocity_angular[i])


    # print(time)

    pubTrajrviz.publish(path)
    pubTrajctrl.publish(reference_trajectory)
    pubPoses.publish(pose_array)


def Ready(data):
    rospy.loginfo("Trajectory was realized")
    readyFlag = data.ready

    ackmsg = ack()
    ackmsg.ack = ackFlag

    pubAck.publish(ackmsg)

def callback(config, level):
    opts['T'] = config.T
    opts['TOL'] = config.TOL
    opts['gamma0'] = config.gamma
    opts['k_max'] = config.k_max
    opts['dts'] = config.dts

    return config


def node():
    global pubAck
    global pubTrajrviz
    global pubTrajctrl
    global pubPoses

    rospy.init_node('pseudoinverse_planner')

    # Publishers
    pubAck = rospy.Publisher("ack", Bool, queue_size=100)
    pubTrajrviz = rospy.Publisher("trajectory_rviz", Path, queue_size=100)
    pubTrajctrl = rospy.Publisher("trajectory_controller", Float64MultiArray, queue_size=100)
    pubPoses = rospy.Publisher("poses", PoseArray, queue_size=100)

    # Subscribers
    rospy.Subscriber("move_base_simple/goal", PoseStamped, DesiredPlan)
    rospy.Subscriber("ready", Bool, Ready)

    srv = Server(PseudoinverseConfig, callback)

    rospy.spin()


if __name__ == '__main__':
    try:
        node()
    except rospy.ROSInterruptException:
        pass
