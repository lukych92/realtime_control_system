#ifndef OROCOS_SHOWER_COMPONENT_HPP
#define OROCOS_SHOWER_COMPONENT_HPP


/*****************************************************************************
 *                                                                           *
 *   shower.hpp                                                              *
 *                                                                           *
 *   Shower -- showing the  relevant values  from the Motion Capture         *
 *                                                                           *
 *   Copyright (C) 2014 by Mateusz Cholewinski                               *
 *   mateusz.cholewinski@pwr.edu.pl                                          *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

/*****************************************************************************
 * TODO: Optimize and add another functionalities.
 *****************************************************************************
 *
 *
 *
 * Shower.cpp has been transformed form SimpleEcample.cpp,  a part of NatNetLinux
 * and is Copyright 2013-2014, Philip G. Lee <rocketman768@gmail.com>
 */


/*
 *
 *  This file defines the Shower class, which models the concept of the
 *  Motion Capture system, providing desired information (it depends on the
 *  class interfaces).
 */


/*****************************************************************************
 * Includes not used yet
 *****************************************************************************/

//#include <rtt/TaskContext.hpp>
//#include <rtt/Activity.hpp>
//#include <rtt/Attribute.hpp>

/*****************************************************************************
 * Used includes
 *****************************************************************************/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

/*****************************************************************************
 * Includes from NatNetLinux framework
 *****************************************************************************/

#include "NatNet.h"
#include "CommandListener.h"
#include "FrameListener.h"

#include <time.h>

/*
 *  It models the concept of the Motion Capture system, providing
 *  desired information (it dependd on the class interfaces).
 *
 *  It has 10 attributes but the most important are the commandListener
 *  and frameListener, which are the implementation of threads listening
 *  for the commands and listening for the frames form the MoCap Server
 */


class Shower
{
public:

	/*****************************************************************************
	 *
	 * Attributes:
	 *
	 * localAddress - address of the host computer, written in the inet_addr type
	 *
	 * serverAddress - address of the server computer, written in the inet_addr type
	 *
	 * sdCommand - socket for commands
	 *
	 * sdData - socket for data
	 *
	 * ping - ping packet send to the server for gaining the NatNet version
	 *
	 * natNetMajor - version number of the NatNet protocol, as reported by the server
	 *
	 * natNetMinor - version number of the NatNet protocol, as reported by the server
	 *
	 * commandListener - pointer for the CommandListener class, modeling the thread
	 * 						for listening commands form the server
	 *
	 * frameListener - pointer for the FrameListener class, modeling the thread
	 * 						for listening data form the server
	 *
	 *
	 *****************************************************************************/

	/*****************************************************************************
	 * localAddress - address of the host computer, written in the inet_addr type
	 * *****************************************************************************/

	uint32_t localAddress;


	/*****************************************************************************
	 * serverAddress - address of the server computer, written in the inet_addr type
	 * *****************************************************************************/

	uint32_t serverAddress;

	/*****************************************************************************
	 * sdCommand - socket for commands
	 * *****************************************************************************/

	int sdCommand;

	/*****************************************************************************
	 * sdData - socket for data
	 * *****************************************************************************/

	int sdData;

	/*****************************************************************************
	 * ping - ping packet send to the server for gaining the NatNet version
	 * *****************************************************************************/

	NatNetPacket ping;

	/*****************************************************************************
	 * run - indicates if the threads are running
	 * *****************************************************************************/

	bool run;

	/*****************************************************************************
	 * natNetMajor - version number of the NatNet protocol, as reported by the server
	 * *****************************************************************************/

	unsigned char natNetMajor;

	/*****************************************************************************
	 * natNetMinor - version number of the NatNet protocol, as reported by the server
	 * *****************************************************************************/

	unsigned char natNetMinor;

	/*****************************************************************************
	 * commandListener - pointer for the CommandListener class, modeling the thread
	 * 						for listening commands form the server
	 * *****************************************************************************/

	CommandListener *commandListener;

	/*****************************************************************************
	 * frameListener - pointer for the FrameListener class, modeling the thread
	 * 						for listening data form the server
	 * *****************************************************************************/
	FrameListener *frameListener;



	/*****************************************************************************
	 * Function prototypes
	 *****************************************************************************/


	/*****************************************************************************
	 *
	 * Initialize Shower class.
	 *
	 *	It:
	 *	- gives the value for the host and server IP addresses,
	 *	- creates the socket for both threads,
	 *	- sends the sing signal to the server in order to get the server version,
	 *	- dynamically creates the threads for listening the data and commands.
	 *
	 *
	 *****************************************************************************/


	Shower();

	/*****************************************************************************
	 *
	 * Get the actual value of run attribute.
	 *
	 *****************************************************************************/

	bool getRun();

	/*****************************************************************************
	 *
	 * Set run attribute.
	 *
	 * \param[in] val	The variable containing the boolean value
	 *
	 *****************************************************************************/

	void setRun(bool val);


	/*****************************************************************************
	 *
	 * It prints all the data send by the server on the screen.
	 *
	 * \param[in] fralist	thread for listening the data from the server
	 *
	 *****************************************************************************/

	// This thread loop just prints frames as they arrive.
	bool printFrames(FrameListener& fralist, std::vector<double>& );

	/*****************************************************************************
	 *
	 * It is responsible for doing some statistics about the obtained frames.
	 * NOT USED NOW!
	 *
	 * \param[in] val	The variable containing the boolean value
	 *
	 *****************************************************************************/

	// This thread loop collects inter-frame arrival statistics and prints a
	// histogram at the end. You can plot the data by copying it to a file
	// (say time.txt), and running gnuplot with the command:
	//     gnuplot> plot 'time.txt' using 1:2 title 'Time Stats' with bars
	//	void timeStats(FrameListener& , const float , const float , const int );

};


#endif



