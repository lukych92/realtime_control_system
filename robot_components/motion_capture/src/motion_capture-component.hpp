#ifndef OROCOS_MOTION_CAPTURE_COMPONENT_HPP
#define OROCOS_MOTION_CAPTURE_COMPONENT_HPP

#include "Shower.cpp"

#include <rtt/RTT.hpp>
//#include <std_msgs/Int16.h>
#include <std_msgs/Float64MultiArray.h>
#include <vector>

class MotionCapture : public RTT::TaskContext{
public:

	RTT::OutputPort <std::vector<double> > StanMC;
	RTT::OutputPort <std_msgs::Float64MultiArray> StanMCROS;

	/*****************************************************************************
	 *
	 * Attribute:
	 *
	 * mc - Shower object, representing the Motion Capture System
	 *

	 *****************************************************************************/

	Shower mc;
	std::vector<double> daneMC;

	/*****************************************************************************
	 *
	 * Initialize MotionCaptureComponent class.
	 *
	 *
	 *****************************************************************************/

	MotionCapture(std::string const& name);
	bool configureHook();


	/*****************************************************************************
	 *
	 * Method which starts the component.
	 *
	 * It also starts the Shower class threads responsible for communication with
	 * Server
	 *
	 *
	 *****************************************************************************/

	bool startHook();
	void updateHook();

	/*****************************************************************************
	 *
	 * Method printing obtained data.
	 *
	 *****************************************************************************/


	void stopHook();

	/*****************************************************************************
	 *
	 * Method which stops the component.
	 *
	 * It stops all threads
	 *
	 *****************************************************************************/


	void cleanupHook();

	//void rewriteFrame(FrameListener, std::vector<double>);
};
#endif
