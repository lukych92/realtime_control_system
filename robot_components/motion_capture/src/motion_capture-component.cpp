#include "motion_capture-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>

#define DEBUG


MotionCapture::MotionCapture(std::string const& name) :
TaskContext(name),
StanMC("stanMC"),
StanMCROS("stanMCROS")
{
	std::cout << "MotionCapture constructed !" <<std::endl;

	this->ports()->addPort("stanMC", StanMC).doc("input port \n stanMC");
	this->ports()->addPort("stanMCROS", StanMCROS).doc("input port \n stanMC");

}

bool MotionCapture::configureHook(){
	std::cout << "MotionCapture configured !" <<std::endl;
	return true;
}


bool MotionCapture::startHook(){
  volatile  int i=0;
	std::cout << "MotionCapture started !" <<std::endl;

	struct sockaddr_in serverCommands = NatNet::createAddress(mc.serverAddress, NatNet::commandPort);

	std::cout << "Listener starts !" <<std::endl;
	mc.commandListener->start();

	// Send a ping packet to the server so that it sends us the NatNet version
	// in its response to commandListener.

	std::cout << "Ping send!" <<std::endl;
	mc.ping.send(mc.sdCommand, serverCommands);


	// Wait here for ping response to give us the NatNet version.

	//mc.commandListener->getNatNetVersion(mc.natNetMajor, mc.natNetMinor);

	// Start up a FrameListener in a new thread.

	std::cout << "frame list !" <<std::endl;

	mc.frameListener->start();

	std::cout << "MotionCapture started !" <<std::endl;

	while (i<1000000)
	  i++;

	return true;
}

void MotionCapture::updateHook(){
  //std::cout << "MotionCapture executes updateHook !" <<std::endl;
	//	std::cout << "MotionCapture started ! COnsider it done!" <<std::endl;

	/*
	daneMC[0] -- X position
	daneMC[1] -- Y position
	daneMC[2] -- Theta orientation
	daneMC[3] -- latency
	daneMC[4] -- hour
	daneMC[5] -- min
	daneMC[6] -- sec
	daneMC[7] -- fframe
	daneMC[8] -- subframe
	 */
	//std::cout<< "daneMC w oper x " << daneMC[0] << std::endl;


	//  << "  Rigid Bodies: " << size << std::endl;
	// for( i = 0; i < size; ++i )
	//   s << rBodies[i];

	std_msgs::Float64MultiArray daneMCROS;
	daneMCROS.data.resize(3);

	if (mc.printFrames(*mc.frameListener, daneMC))
	{
     //std::cout << "dane x: " << daneMC[0] << std::endl;
     // std::cout << "dane y: " << daneMC[1] << std::endl;
     // std::cout << "dane z: " << daneMC[2] << std::endl;

        for(unsigned int i=0;i<3;++i)
			daneMCROS.data[i]=daneMC[i];

		StanMC.write(daneMC);
		StanMCROS.write(daneMCROS);
	}

	//rewriteFrame(*mc.frameListener, daneMC);



}

void MotionCapture::stopHook() {
	std::cout << "MotionCapture executes stopping !" <<std::endl;

	mc.frameListener->stop();
	mc.commandListener->stop();
	mc.frameListener->join();
	mc.commandListener->join();

	// Epilogue
	close(mc.sdData);
	close(mc.sdCommand);

}

void MotionCapture::cleanupHook() {
	std::cout << "MotionCapture cleaning up !" <<std::endl;

}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(MotionCapture)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(MotionCapture)
