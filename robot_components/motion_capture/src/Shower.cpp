
/*****************************************************************************
 *                                                                           *
 *   shower.cpp                                                              *
 *                                                                           *
 *   Shower -- showing the  relevant values  from the Motion Capture         *
 *                                                                           *
 *   Copyright (C) 2014 by Mateusz Cholewinski                               *
 *   mateusz.cholewinski@pwr.edu.pl                                              *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.               *
 *                                                                           *
 *****************************************************************************/

/*****************************************************************************
 * TODO: Optimize and add another functionalities.
 *****************************************************************************
 *
 *
 *
 * Shower.cpp was a part of NatNetLinux known as SimpleEcample.cpp,
 * and is Copyright 2013-2014, Philip G. Lee <rocketman768@gmail.com>
 */

#include "Shower.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


Shower::Shower(){

	//std::string local="169.254.4.16";
	//std::string server="169.254.4.15";

	std::string local="192.168.0.90";
	std::string server="192.168.0.60";

	localAddress=inet_addr(local.c_str() );
	serverAddress=inet_addr(server.c_str() );

	run=false;

	sdCommand = NatNet::createCommandSocket( localAddress );
	sdData = NatNet::createDataSocket( localAddress );

	ping = NatNetPacket::pingPacket();

	commandListener = new CommandListener(sdCommand); // creating thread commandListener
	frameListener = new FrameListener(sdData, natNetMajor, natNetMinor); // creating thread FrameListener
	//FrameListener frameListener(sdData, natNetMajor, natNetMinor);

}

bool Shower::getRun() {
	return run;
}

void Shower::setRun(bool aa){
	run=aa;
}


// This thread loop just prints frames as they arrive.
bool Shower::printFrames(FrameListener& frameListener, std::vector<double>& dane)
{
	bool valid;
	Shower::setRun(true);
	//MocapFrame frame;

	//	while(Shower::getRun())
	//	{
	//	while( true )
	//	{


	// Try to get a new frame from the listener.
	MocapFrame frame(frameListener.pop(&valid).first);
	// Quit if the listener has no more frames.
	if( valid ){
	//	std::cout << frame << std::endl;
		dane << frame;
		//std::cout << "dane x: " << dane[0] << std::endl;
	}


	//	}

	// Sleep for a little while to simulate work :)
	//usleep(1000);
	//}

	return valid;
}


// This thread loop collects inter-frame arrival statistics and prints a
// histogram at the end. You can plot the data by copying it to a file
// (say time.txt), and running gnuplot with the command:
//     gnuplot> plot 'time.txt' using 1:2 title 'Time Stats' with bars
//void Shower::timeStats(FrameListener& frameListener, const float diffMin_ms = 0.5, const float diffMax_ms = 7.0, const int bins = 100)
//{
//	size_t hist[bins];
//	float diff_ms;
//	int bin;
//	struct timespec current;
//	struct timespec prev;
//	struct timespec tmp;
//
//	std::cout << std::endl << "Collecting inter-frame arrival statistics...press ctrl-c to finish." << std::endl;
//
//	memset(hist, 0x00, sizeof(hist));
//	bool valid;
//	Shower::run = true;
//	while(Shower::run)
//	{
//		while( true )
//		{
//			// Try to get a new frame from the listener.
//			prev = current;
//			tmp = frameListener.pop(&valid).second;
//			// Quit if the listener has no more frames.
//			if( !valid )
//				break;
//
//			current = tmp;
//
//			diff_ms =
//					std::abs(
//							(static_cast<float>(current.tv_sec)-static_cast<float>(prev.tv_sec))*1000.f
//							+ (static_cast<float>(current.tv_nsec)-static_cast<float>(prev.tv_nsec))/1000000.f
//					);
//
//			bin = (diff_ms-diffMin_ms)/(diffMax_ms-diffMin_ms) * (bins+1);
//			if( bin < 0 )
//				bin = 0;
//			else if( bin >= bins )
//				bin = bins-1;
//
//			hist[bin] += 1;
//		}
//
//		// Sleep for a little while to simulate work :)
//		usleep(1000);
//	}
//
//	// Print the stats
//	std::cout << std::endl << std::endl;
//	std::cout << "# Time diff (ms), Count" << std::endl;
//	for( bin = 0; bin < bins; ++bin )
//		std::cout << diffMin_ms+(diffMax_ms-diffMin_ms)*(0.5f+bin)/bins << ", " << hist[bin] << std::endl;
//}




//ORO_LIST_COMPONENT_TYPE( MotionCaptureComponent );

