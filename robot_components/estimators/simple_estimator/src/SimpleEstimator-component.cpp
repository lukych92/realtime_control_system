#include "SimpleEstimator-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>
#include <cmath>


#define DEG2RAD (M_PI/180)

static const int WhFilterLen = 2;
static const int XYFilterLen = 10;
static const int PhiFilterLen = 10;

SimpleEstimator::SimpleEstimator(std::string const& name) :
  TaskContext(name)
//  WhFilter1(WhFilterLen,true), WhFilter2(WhFilterLen,true),
//  WhFilter3(WhFilterLen,true), WhFilter4(WhFilterLen,true),
//  XFilter(XYFilterLen, false), YFilter(XYFilterLen, false),
//  PhiFilter(PhiFilterLen, false)
{
  std::cout << "SimpleEstimator constructed !" <<std::endl;
  this->ports()->addPort( "WheelsPos", WheelsPos )
    .doc("Input port for wheels positions [rad]" );
  this->ports()->addPort( "PosFromMC", PosFromMC )
    .doc("Input port for position from MC" );
  this->ports()->addPort( "State", State )
    .doc("Output port for platform state" );

  this->ports()->addPort( "odometry", _odometry );

  _odom_msg.header.frame_id = "odom";
  _odom_msg.child_frame_id = "base_footprint";

  odom_trans.header.frame_id = "odom";
  odom_trans.child_frame_id = "base_link";
  odom_trans.transform.translation.z = 0.0;
  odom_trans.transform.rotation = tf::createQuaternionMsgFromYaw(0);


  _state.data.resize(3);

}

bool SimpleEstimator::configureHook(){
  std::cout << "SimpleEstimator configured !" <<std::endl;
  return true;
}

bool SimpleEstimator::startHook()
{
  InitPosRead = false;
  InitWhPosRead =false;
  WhPosRead = false;
  MCPosRead = false;
//  WhFilter1.Reset();
//  WhFilter2.Reset();
//  WhFilter3.Reset();
//  WhFilter4.Reset();
//  XFilter.Reset();
//  YFilter.Reset();
//  PhiFilter.Reset();

  PrevPhiFromMC = 0;
  PhiAcc = 0;

  if(!InitPosRead || !InitWhPosRead)
  {
    std_msgs::Float64MultiArray Pos;
    if(PosFromMC.readNewest(Pos) == RTT::NewData){
      if(Pos.data.size() < 3){
	std::cout << "Bad MC position vector size!" << std::endl;
//	return false;
      }
      InitialPos.data = Pos.data;
      InitPosRead = true;
      std::cout << "Initial platform position read" << std::endl;
      std::cout << "phi [deg]: " << InitialPos.data[2] << std::endl;
    } 
    std_msgs::Float64MultiArray WhPos;
    if(WheelsPos.readNewest(WhPos) == RTT::NewData){
      if(WhPos.data.size() != 2){
	std::cout << "Bad wheels position vector size!" << std::endl;
//	return false;
      }
      InitialWhPos.data = WhPos.data;
      InitWhPosRead = true;
      std::cout << "Initial wheels positions read" << std::endl;
    }
//    if(!InitPosRead || !InitWhPosRead)
//      return false;
  }

  std::cout << "SimpleEstimator started !" <<std::endl;
  return true;
}

void SimpleEstimator::updateHook()
{

//  std_msgs::Float64MultiArray WhPos;
//  platform_state_msgs::PlatformState PState;

//  double Period=this->getPeriod();
//  if(WheelsPos.readNewest(WhPos) == RTT::NewData){
//    WhPosRead = true;
//    if(WhPos.data.size() != 2){
//      std::cout << "Bad wheels position vector size!" << std::endl;
//      return;
//    }
//    PState.theta1 = WhPos.data[0] - InitialWhPos.data[0];
//    PState.theta2 = WhPos.data[1] - InitialWhPos.data[1];
//    PState.theta3 = 0;
//    PState.theta4 = 0;
//    PState.dtheta1 = WhFilter1.CalcSpeed(WhPos.data[0])/Period;
//    PState.dtheta2 = WhFilter2.CalcSpeed(WhPos.data[1])/Period;
//    PState.dtheta3 = 0;
//    PState.dtheta4 = 0;
//  }
  if(InitPosRead)
  {
      std_msgs::Float64MultiArray Pos;
      if(PosFromMC.readNewest(Pos) == RTT::NewData){
        MCPosRead = true;
        if(Pos.data.size() < 3){
          std::cout << "Bad MC position vector size!" << std::endl;
          return;
        }
        double xt = Pos.data[0] - InitialPos.data[0];
        double yt = Pos.data[1] - InitialPos.data[1];
        double phi0 = InitialPos.data[2]*DEG2RAD;
        double PhiFromMC = Pos.data[2]*DEG2RAD;
        if(PhiFromMC - PrevPhiFromMC < -M_PI)
          PhiAcc += 2*M_PI;
        if(PhiFromMC - PrevPhiFromMC > M_PI)
          PhiAcc -= 2*M_PI;
        PrevPhiFromMC = PhiFromMC;


        _state.data[0] = xt;//xt*cos(phi0) + yt*sin(phi0); //++ //reference frame transf.
        _state.data[1] = yt;//-xt*sin(phi0) + yt*cos(phi0);//-+
        _state.data[2] = PhiFromMC + PhiAcc - phi0;


        _odom_msg.pose.pose.position.x = xt;
        _odom_msg.pose.pose.position.y = yt;
        _odom_msg.pose.pose.position.z = 0;
        _odom_msg.pose.pose.orientation = tf::createQuaternionMsgFromYaw(_state.data[2]);


        odom_trans.transform.translation.x = xt;
        odom_trans.transform.translation.y = yt;
        odom_trans.transform.rotation = tf::createQuaternionMsgFromYaw(_state.data[2]);
//        tf_.broadcastTransform(odom_trans);
      }
  }
//  else
//  {
//          std::cout << "Nie odczytalem MC" <<std::endl;
//  }
//  if(MCPosRead && WhPosRead)
//    PState.isValid = 1;
//  else{
//    if(!MCPosRead)
//      std::cout << "Nie odczytalem MC" <<std::endl;
//    if(!WhPosRead)
//      std::cout << "Nie odczytalem enkoderow" <<std::endl;
//    PState.isValid = 0;
//  }
  State.write(_state);
  _odom_msg.header.stamp = ros::Time::now();
  _odometry.write(_odom_msg);
  odom_trans.header.stamp = ros::Time::now();

  broadcaster.sendTransform(odom_trans);

  //std::cout << "SimpleEstimator executes updateHook !" <<std::endl;

}

void SimpleEstimator::stopHook() {
//  InitPosRead = false;
//  InitWhPosRead =false;
  std::cout << "SimpleEstimator executes stopping !" <<std::endl;
}

void SimpleEstimator::cleanupHook() {
  std::cout << "SimpleEstimator cleaning up !" <<std::endl;
}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(SimpleEstimator)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(SimpleEstimator)
