#ifndef OROCOS_SIMPLEESTIMATOR_COMPONENT_HPP
#define OROCOS_SIMPLEESTIMATOR_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <vector>

//#include <platform_state_msgs/PlatformState.h>

//#include "MovingAverage.hpp"
#include <std_msgs/Float64MultiArray.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>


#include <tf/tf.h>

class SimpleEstimator : public RTT::TaskContext{
public:
  SimpleEstimator(std::string const& name);
  bool configureHook();
  bool startHook();
  void updateHook();
  void stopHook();
  void cleanupHook();
  
  RTT::InputPort<std_msgs::Float64MultiArray > WheelsPos;
  RTT::InputPort<std_msgs::Float64MultiArray > PosFromMC;
//  RTT::OutputPort<platform_state_msgs::PlatformState> State;
  RTT::OutputPort<std_msgs::Float64MultiArray> State;
  RTT::OutputPort<nav_msgs::Odometry> _odometry;
  tf::TransformBroadcaster broadcaster;


private:
//  DiffFilter WhFilter1;
//  DiffFilter WhFilter2;
//  DiffFilter WhFilter3;
//  DiffFilter WhFilter4;
//  DiffFilter XFilter;
//  DiffFilter YFilter;
//  DiffFilter PhiFilter;

  bool InitPosRead;
  bool InitWhPosRead;
  bool WhPosRead;
  bool MCPosRead;
  std_msgs::Float64MultiArray InitialPos;
  std_msgs::Float64MultiArray InitialWhPos;
  std_msgs::Float64MultiArray _state;
  nav_msgs::Odometry          _odom_msg;
  geometry_msgs::TransformStamped odom_trans;



  double PrevPhiFromMC;
  double PhiAcc;
};
#endif
