#include "wheels_module-component.hpp"
#include <rtt/Component.hpp>
#include <iostream>
#include <math.h>



const double Wheels_module::Period=0.01;
const double Wheels_module::TimeoutTime=0.2;


Wheels_module::Wheels_module(std::string const& name) :
  TaskContext(name), TimeoutCntr(0), TorqueShift(0),
  motor_left(0,0,0,0), motor_right(0,0,0,0), tf_(this)
{

    TicksPerRevolution = 76000;
    WheelRadius = 0.195/2;//meters
    DistanceBetweenWheels = 0.381;//meters
    VoltageConstant = 1.25;

  this->ports()->addPort( "motor_left_desired_vel", _motor_left_desired_vel );
  this->ports()->addPort( "motor_left_actual_vel", _motor_left_actual_vel );
  this->ports()->addPort( "motor_left_control", _motor_left_control );
  this->ports()->addPort( "motor_right_desired_vel", _motor_right_desired_vel );
  this->ports()->addPort( "motor_right_actual_vel", _motor_right_actual_vel );
  this->ports()->addPort( "motor_right_control", _motor_right_control );

  this->ports()->addPort( "joints_state", _joints_state )
    .doc("Output port for measured wheels positions" );
    this->ports()->addPort( "controls", _controls )
    .doc("Input port for wheels control signals" );

  this->ports()->addPort( "rtnode_send", _RTnode_send )
      .doc("send message to rtnode" );
  this->ports()->addPort("rtnode_recv", _RTnode_recv).
          doc("receive msg from rtnode");

    this->addProperty("motor_left_P",motor_left_P);
    this->addProperty("motor_left_I",motor_left_I);
    this->addProperty("motor_left_D",motor_left_D);
    this->addProperty("motor_left_F",motor_left_F);

    this->addProperty("motor_right_P",motor_right_P);
    this->addProperty("motor_right_I",motor_right_I);
    this->addProperty("motor_right_D",motor_right_D);
    this->addProperty("motor_right_F",motor_right_F);
    this->addProperty("outputFilter",outputFilter);
    this->addProperty("outputLimits",outputLimits);

  this->addProperty("TicksPerRevolution",TicksPerRevolution);
  this->addProperty("WheelRadius",WheelRadius).doc("in meters");
  this->addProperty("DistanceBetweenWheels",DistanceBetweenWheels).doc("in meters");
  this->addProperty("VoltageConstant",VoltageConstant).doc("rad/s/V");

        rosparam =  this->getProvider<rtt_rosparam::ROSParam>("rosparam");


addOperation("setL_PID", 
           &Wheels_module::setL_PID, 
           this, 
           RTT::OwnThread);


addOperation("setR_PID", 
           &Wheels_module::setR_PID, 
           this, 
           RTT::OwnThread);


addOperation("set_cutoff_filter", 
           &Wheels_module::set_cutoff_filter, 
           this, 
           RTT::OwnThread);

    ActualVelocity.data.resize(2);
    DemandVelocity.resize(2);
    control_value.resize(2);

    verbose = true;

    joints_states_.header.frame_id = "base_link";
    joints_states_.name.push_back("base_left_wheel_joint");
    joints_states_.name.push_back("base_right_wheel_joint");
    joints_states_.position.resize(2,0.0);
    joints_states_.velocity.resize(2,0.0);
//    joints_states_.effort.resize(2,0.0);



    outputLimits = 10;
  
    motor_left.setOutputLimits(outputLimits);
    motor_right.setOutputLimits(outputLimits);
    motor_left.setMaxIOutput(10);


  std::cout << "Wheels_module constructed !" <<std::endl;
}

bool Wheels_module::configureHook(){
    bool all_params_found = true;


    // Get the parameters


    return all_params_found;

  //return this->setPeriod(Period);
  std::cout << "Wheels_module configured !" <<std::endl;
  return true;
}

bool Wheels_module::startHook(){
  motor_left.reset();
  motor_right.reset();
  actual_vel_left_filter.setCutOffFrequency(250.0);
  actual_vel_left_filter.setDeltaTime(0.001);
  actual_vel_right_filter.setCutOffFrequency(250.0);
  actual_vel_right_filter.setDeltaTime(0.001);
/*
  if(rosparam)
  {
    rosparam->getAll();
  }
*/
/*
  motor_left.setPID(motor_left_P,
                    motor_left_I,
                    motor_left_D,
                    motor_left_F
                    );
  motor_right.setPID(motor_right_P,
                     motor_right_I,
                     motor_right_D,
                     motor_right_F
                     );
*/
 // motor_right.setOutputFilter(outputFilter);
 // motor_right.setOutputLimits(-outputLimits,outputLimits);
  TimeoutCntr=0;
  ZeroDACs();
  std::cout << "Wheels_module started !" <<std::endl;
  return tf_.ready();
}

void Wheels_module::updateHook(){


  // subscribe actual joints state
  if(_RTnode_recv.readNewest(msg_recv) == RTT::NewData)
  {
//      double tmp_L = 0,tmp_R=0;
/*
      ActualVelocity.data[0] = msg_recv.dir0*TicksToRad((double)msg_recv.tic0/msg_recv.t0*1000000000);//rad/s
      ActualVelocity.data[1] = -msg_recv.dir1*TicksToRad((double)msg_recv.tic1/msg_recv.t1*1000000000);//rad/s
*/
      ActualVelocity.data[0] = actual_vel_left_filter.update(
                  msg_recv.dir0*TicksToRad((double)msg_recv.tic0/msg_recv.t0*1000000000));//rad/s
      ActualVelocity.data[1] = actual_vel_right_filter.update(
                  -msg_recv.dir1*TicksToRad((double)msg_recv.tic1/msg_recv.t1*1000000000));//rad/s


//      tmp_L = (ActualVelocity.data[0]+joints_states_.velocity[0])/2;
//      tmp_R = (ActualVelocity.data[1]+joints_states_.velocity[1])/2;

      joints_states_.position[0] = TicksToRad(msg_recv.pos0);
      joints_states_.position[1] = TicksToRad(msg_recv.pos1);
      joints_states_.velocity[0] = ActualVelocity.data[0];
      joints_states_.velocity[1] = ActualVelocity.data[1];


//      ActualVelocity.data[0] = tmp_L;
//      ActualVelocity.data[1] = tmp_R;
  }

  // subscribe demand velocities
  if(_controls.readNewest(controls_msg) == RTT::NewData){
    TimeoutCntr = 0; //clear watchdog 

    //inverse kinematics

    DemandVelocity[0] = (controls_msg.linear.x-controls_msg.angular.z*DistanceBetweenWheels/2)/WheelRadius;//rad/s, left motor angular vel
    DemandVelocity[1] = (controls_msg.linear.x+controls_msg.angular.z*DistanceBetweenWheels/2)/WheelRadius;//rad/s, right motor angular vel
  }

//    control_value[0] = DemandVelocity[0];
//    control_value[1] = DemandVelocity[1];

    // compute control signals
    control_value[0] = motor_left.getOutput(ActualVelocity.data[0],DemandVelocity[0]);
    control_value[1] = motor_right.getOutput(ActualVelocity.data[1],DemandVelocity[1]);


    // Publish all
    joints_states_.header.stamp = ros::Time::now();
    _joints_state.write(joints_states_);

    WriteDACs(ControlToDAC(control_value[0]),//left motor
              ControlToDAC(control_value[1])//right motor
              );


    std_msgs::Float64 msg;  
    msg.data = DemandVelocity[0];
    _motor_left_desired_vel.write(msg);
    msg.data = ActualVelocity.data[0];
     _motor_left_actual_vel.write(msg);
    msg.data = control_value[0];
     _motor_left_control.write(msg);

    msg.data = DemandVelocity[1];
    _motor_right_desired_vel.write(msg);
    msg.data = ActualVelocity.data[1];
    _motor_right_actual_vel.write(msg);
    msg.data = control_value[1];
     _motor_right_control.write(msg);



  TimeoutCntr++;
  //If timeout expired, switch off motors
  if(TimeoutCntr >= TimeoutTime/Period)
  {
    ZeroDACs();
    verbose = false;
  }
  else verbose = true;

//  ros::spinOnce();
}

void Wheels_module::stopHook() {
  ZeroDACs();
  std::cout << "Wheels_module executes stopping !" <<std::endl;
}

void Wheels_module::cleanupHook() {
  ZeroDACs();
  std::cout << "Wheels_module cleaning up !" <<std::endl;
}

void Wheels_module::WriteDACs(int left_motor, int right_motor)//0-4080
{
    sMsgRTnodeBaseOut_t msg_send;



    msg_send.id = 0;
    msg_send.gid = 0;
    msg_send.ch00 = right_motor;
    msg_send.ch01 = left_motor;
    msg_send.o0 = 1;
    msg_send.o1 = 1;
    msg_send.mask = 15;


    _RTnode_send.write(msg_send);

}

void Wheels_module::ZeroDACs()
{
    WriteDACs(ControlToDAC(0.0),ControlToDAC(0.0));
}

int Wheels_module::ControlToDAC(double Control )//Volts to DAC
{
    const double MAX_VOLTAGE = 10;

//    double Voltage = Control/VoltageConstant; // rad/s to Voltage

   if(Control > MAX_VOLTAGE) Control = MAX_VOLTAGE;
   if(Control < -MAX_VOLTAGE) Control = -MAX_VOLTAGE;

    return (int)(204*Control+2048);// Voltage to DAC
}

/*
 * Using this macro, only one component may live
 * in one library *and* you may *not* link this library
 * with another component library. Use
 * ORO_CREATE_COMPONENT_TYPE()
 * ORO_LIST_COMPONENT_TYPE(Wheels_module)
 * In case you want to link with another library that
 * already contains components.
 *
 * If you have put your component class
 * in a namespace, don't forget to add it here too:
 */
ORO_CREATE_COMPONENT(Wheels_module)
