#ifndef OROCOS_WHEELS_MODULE_PID_ROS_COMPONENT_HPP
#define OROCOS_WHEELS_MODULE_PID_ROS_COMPONENT_HPP

#include <rtt/RTT.hpp>
#include <vector>
#include <cmath>
#include <rtt/OperationCaller.hpp>
#include <rtt_tf/tf_interface.h>
#include <rtt_rosparam/rosparam.h>
#include <rtt_rosclock/rtt_rosclock.h>

#include "msgRTnodeBaseOut.h"
#include "msgRTnodeBaseIn.h"

#include <std_msgs/Float64MultiArray.h>
#include <std_msgs/Float64.h>

#include <geometry_msgs/Twist.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf/transform_broadcaster.h>

#include "MiniPID.h"
#include "LowPassFilter.hpp"

class Wheels_module_pid_ros : public RTT::TaskContext{
public:
  Wheels_module_pid_ros(std::string const& name);
  bool configureHook();
  bool startHook();
  void updateHook();
  void stopHook();
  void cleanupHook();

  RTT::InputPort<geometry_msgs::Twist > _controls;
  RTT::OutputPort<sensor_msgs::JointState > _joints_state;
  tf::TransformBroadcaster broadcaster;


  RTT::OutputPort<std_msgs::Float64 > _motor_left_actual_vel;
  RTT::InputPort<std_msgs::Float64 > _motor_left_control;

  RTT::OutputPort<std_msgs::Float64 > _motor_right_actual_vel;
  RTT::InputPort<std_msgs::Float64 > _motor_right_control;

  RTT::InputPort<sMsgRTnodeBaseIn_t > _RTnode_recv;
  RTT::OutputPort<sMsgRTnodeBaseOut_t > _RTnode_send;

  static const double Period;
  static const double TimeoutTime;
  
private:
  int TimeoutCntr; //timeout counter
  double TorqueShift; 

    boost::shared_ptr<rtt_rosparam::ROSParam> rosparam;

  double WheelRadius;
  double DistanceBetweenWheels;
  int TicksPerRevolution;

  rtt_tf::TFInterface tf_;
  sMsgRTnodeBaseIn_t msg_recv;
  geometry_msgs::Twist controls_msg;
  geometry_msgs::TransformStamped odom_trans;
  double VoltageConstant;

  double motor_left_P, motor_left_I, motor_left_D, motor_left_F;
  double motor_right_P, motor_right_I, motor_right_D, motor_right_F;
  double outputFilter, outputLimits;

  std::vector<double> control_value;




  void WriteDACs(int left_motor, int right_motor);
  void ZeroDACs();
  double TicksToRad(int EncTicks)
  {
    const double RadPerTick = 2*M_PI/TicksPerRevolution;
    return EncTicks*RadPerTick;
  }
  int ControlToDAC(double Control);

  void setL_PID(double P,double I, double D)
  {
    motor_left.setPID(P,
                      I,
                      D,
                      0
                      );

  motor_right.setPID(P,
                     I,
                     D,
                     0
                     );

  }

  void setR_PID(double P,double I, double D)
  {

    motor_left.setPID(P,
                      I,
                      D,
                      0
                      );

  motor_right.setPID(P,
                     I,
                     D,
                     0
                     );
  }

  sensor_msgs::JointState joints_states_;


  std_msgs::Float64MultiArray ActualVelocity;
  std::vector<double> DemandVelocity;



  bool verbose;

  MiniPID motor_left;
  MiniPID motor_right;
  LowPassFilter actual_vel_left_filter;
  LowPassFilter actual_vel_right_filter;
};
#endif
