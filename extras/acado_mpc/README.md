# README #

Example of model predictive control code generation. Kinematic monocycle model is used.

Compilation

```
#!bash

mkdir build
cd build
cmake ..
make
cd ..

```

Code generation and test generated code

```
#!bash

./mpc_code_generation
cd mpc_mpc_export
make
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)
./test
```


Simulation
```
#!bash

./mpc_simulation
```