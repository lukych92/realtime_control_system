#include "mpc_common.h"
#include <string.h>

ACADOvariables acadoVariables;
ACADOworkspace acadoWorkspace;

void MPC_preparationStep()
{
  acado_preparationStep();
}

int MPC_feedbackStep()
{
  return acado_feedbackStep();
}

int MPC_initializeSolver()
{
  return acado_initializeSolver();
}

void MPC_initializeNodesByForwardSimulation()
{
  acado_initializeNodesByForwardSimulation();
}

void MPC_shiftStates( int strategy, real_t* const xEnd, real_t* const uEnd )
{
  acado_shiftStates(strategy, xEnd, uEnd);
}

void MPC_shiftControls( real_t* const uEnd )
{
  acado_shiftControls(uEnd);
}

real_t MPC_getKKT()
{
  return acado_getKKT();
}

real_t MPC_getObjective()
{
  return acado_getObjective();
}

void MPC_resetSolverMem()
{
  // Reset all solver memory
  memset(&acadoWorkspace, 0, sizeof( acadoWorkspace ));
  memset(&acadoVariables, 0, sizeof( acadoVariables ));
}
