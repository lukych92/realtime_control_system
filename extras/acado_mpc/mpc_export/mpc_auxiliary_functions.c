#include "acado_common.h"
#include "mpc_auxiliary_functions.h"

real_t* MPC_getAcadoVariablesX( )
{
  return acadoVariables.x;
}

real_t* MPC_getAcadoVariablesU( )
{
  return acadoVariables.u;
}

real_t* MPC_getAcadoVariablesY( )
{
  return acadoVariables.y;
}

real_t* MPC_getAcadoVariablesYN( )
{
  return acadoVariables.yN;
}

real_t* MPC_getAcadoVariablesX0( )
{
#if ACADO_INITIAL_STATE_FIXED
  return acadoVariables.x0;
#else
  return NULL;
#endif
}
real_t* MPC_getAcadoVariablesOD( )
{
#if ACADO_NOD > 0
  return acadoVariables.od;
#else
  return NULL;
#endif
}

void mpc_tic(acado_timer* t){
  acado_tic(t);
}

real_t mpc_toc(acado_timer* t){
  return acado_toc(t);
}

void mpc_printDifferentialVariables()
{
  acado_printDifferentialVariables();
}

void mpc_printControlVariables()
{
  acado_printControlVariables();
}

int mpc_getNWSR( void )
{
  return acado_getNWSR();
}
