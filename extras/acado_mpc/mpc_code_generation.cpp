#include <acado_gnuplot.hpp>
#include <acado_toolkit.hpp>

USING_NAMESPACE_ACADO

int main( )
{
    // Define the dynamic system:
    // -------------------------
    DifferentialState     q1,q2,q3;
    Control               u1,u2;

    //    Simple monocycle
    DifferentialEquation  f;
    f << dot(q1) == cos(q3)*u1;
    f << dot(q2) == sin(q3)*u1;
    f << dot(q3) == u2;

    // DEFINE LEAST SQUARE FUNCTION:
    // -----------------------------
    Function h, hN;

    h << q1 << q2 << q3 << u1 << u2;
//    h << q1 << q2 << q3;
    hN << q1 << q2 << q3;

    DMatrix Q = eye<double>( h.getDim() );
    DMatrix QN = eye<double>( hN.getDim() );

//	Q.setZero();
//	QN.setZero();

    Q(0,0)=10;
    Q(1,1)=10;
    Q(2,2)=10;
    Q(3,3) = 0.005;
    Q(4,4) = 0.005;

    QN(0,0) = 100;
    QN(1,1) = 100;
    QN(2,2) = 10;

    // DEFINE AN OPTIMAL CONTROL PROBLEM:
    // ----------------------------------
    const double dts = 0.01;

    const int N  = 50;
    const int Ni = 50;

    OCP ocp        ( 0, N*dts, N);
    ocp.subjectTo  ( f );
    ocp.minimizeLSQ(Q, h);
    ocp.minimizeLSQEndTerm(QN, hN);

    ocp.subjectTo( -0.5 <= u1 <= 0.5 );
    ocp.subjectTo( -0.5 <= u2 <= 0.5 );

    // Export the code:
    OCPexport mpc( ocp );

    mpc.set(HESSIAN_APPROXIMATION, GAUSS_NEWTON);
    mpc.set(DISCRETIZATION_TYPE, MULTIPLE_SHOOTING);

    mpc.set(INTEGRATOR_TYPE, INT_IRK_RIIA3);
    mpc.set(NUM_INTEGRATOR_STEPS, N * Ni);

//	mpc.set(MAX_NUM_QP_ITERATIONS, 200);
    mpc.set(SPARSE_QP_SOLUTION, FULL_CONDENSING);
//	mpc.set(SPARSE_QP_SOLUTION, CONDENSING);
    mpc.set(QP_SOLVER, QP_QPOASES);
//	mpc.set(MAX_NUM_QP_ITERATIONS, 20);
    mpc.set(HOTSTART_QP, YES);

    mpc.set( GENERATE_TEST_FILE,          NO             );
    mpc.set( GENERATE_MAKE_FILE,          NO             );
    mpc.set( GENERATE_MATLAB_INTERFACE,   NO             );
    mpc.set( GENERATE_SIMULINK_INTERFACE, NO             );


    if (mpc.exportCode( "mpc_export" ) != SUCCESSFUL_RETURN)
        exit( EXIT_FAILURE );
    else
    {
        std::cout << "Code generated to folder mpc_export" << std::endl;
        mpc.printDimensionsQP();
    }


    return 0;
}

