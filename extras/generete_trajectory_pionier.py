#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt

def frange(x, y, jump):
    while x < y:
        yield x
        x += jump


def generete_circle(param):
    T =  param['T']
    dts = param['dts']
    fname = param['fname']


    ## desired trajectory
    R  = param['R']
    w = param['w']

    time = []
    reference_x = []
    reference_y = []
    reference_orientation = []
    velocity_linear = []
    velocity_angular = []

    # for t in np.linspace(0,T,314*4):
    for t in [x for x in frange(0, T, dts)]:
        time.append(t)
        orientation = (w*t)-np.pi/2
        reference_x.append(R*np.cos(orientation))
        reference_y.append(R*np.sin(orientation)+R)
        reference_orientation.append(orientation+np.pi/2)
        velocity_linear.append(param['vel_linear'])
        velocity_angular.append(param['vel_angular'])

    if param['vis'] == True:
        plt.plot(reference_x,reference_y,'k--')
        plt.show()

    f= open(fname, 'w')

    for i in range(len(time)):
        string_line = ' '.join(map(str,[time[i],reference_x[i],reference_y[i],reference_orientation[i],velocity_linear[i],velocity_angular[i]]))
        f.write(string_line+'\n')

if __name__ == "__main__":
    param = {}
    param['T'] = 8*3.14
    param['R'] = 1
    param['w'] = 2*3.14/param['T']
    param['dts'] = 0.01
    param['fname'] = "reference_trajectory.txt"
    param['vis'] = False
    param['vel_linear'] = 0.18
    param['vel_angular'] = 0.18

    generete_circle(param)



