#!/usr/bin/python

import matplotlib.pyplot as plt

if __name__ == "__main__":

    fname = 'realtime_control_system_samson_kinematic_controller_logs_ok.txt'

    lines = tuple(open(fname, 'r'))
    lines = [x.strip() for x in lines]

    time = []
    actual_x = []
    actual_y = []
    actual_orientation = []
    reference_x = []
    reference_y = []
    reference_orientation = []
    velocity_linear = []
    velocity_angular = []


    for i in range(len(lines)):
        # convert to integer and append to the last
        # element of the list
        line = lines[i].split()
        time.append(float(line[0]))
        actual_x.append(float(line[1]))
        actual_y.append(float(line[2]))
        actual_orientation.append(float(line[3]))
        reference_x.append(float(line[4]))
        reference_y.append(float(line[5]))
        reference_orientation.append(float(line[6]))
        velocity_linear.append(float(line[7]))
        velocity_angular.append(float(line[8]))


    for i in range(0,len(time),5):
        name = "gif/trajectory"+str(i)
        # plt.figure()
        plt.plot(reference_x,reference_y,'k--',label='yd')
        plt.plot(actual_x[0:i],actual_y[0:i],'k',label = 'y')
        plt.plot(actual_x[0],actual_y[0],'ok', label="Initial position")
        plt.title('Desired and realized path')
        # plt.legend(loc=0, numpoints=1)
        plt.xlabel('x, xd [m]')
        plt.ylabel('y, yd [m]')
        plt.xlim(-2,2)
        plt.ylim(-1,3)
        plt.savefig(name)
        # plt.show()

    # plt.plot(time,actual_orientation,'k',label='y')
    # plt.plot(time,reference_orientation,'k--',label='yd')
    # plt.show()

