#!/usr/bin/python

import matplotlib.pyplot as plt
import sys
import numpy as np


def plot_from(fname):
    lines = tuple(open(fname, 'r'))
    lines = [x.strip() for x in lines]

    comment = ""
    time = []
    actual_x = []
    actual_y = []
    actual_orientation = []
    reference_x = []
    reference_y = []
    reference_orientation = []
    velocity_linear = []
    velocity_angular = []


    for i in range(len(lines)):
        if lines[i][0] == '#':
            comment += lines[i]+str('\n')
        else:
            # convert to integer and append to the last
            # element of the list
            line = lines[i].split()
            time.append(float(line[0]))
            actual_x.append(float(line[1]))
            actual_y.append(float(line[2]))
            actual_orientation.append(float(line[3]))
            reference_x.append(float(line[4]))
            reference_y.append(float(line[5]))
            reference_orientation.append(float(line[6]))
            velocity_linear.append(float(line[7]))
            velocity_angular.append(float(line[8]))


    print(comment)
    print("Do you want to save figures to files? (T/n)")
    save_to_file = raw_input()
    fig_name = 'example_figure'
    if save_to_file == 'T' or save_to_file == '':
        print("Give the name, please")
        fig_name = raw_input()

    plt.plot(reference_x,reference_y,'k--',label='yd')
    plt.plot(actual_x,actual_y,'k',label = 'y')
    plt.plot(actual_x[0],actual_y[0],'ok', label="Initial position")
    plt.title('Desired and realized path')
    plt.legend(loc=0, numpoints=1)
    plt.xlabel('x, xd [m]')
    plt.ylabel('y, yd [m]')
    if save_to_file == 'T' or save_to_file == '':
        plt.savefig(fig_name+".png",
                    format = 'png'
                    )
        plt.clf()
    else:
        plt.show()

    mse_x = 100*np.sum((np.array(reference_x)-np.array(actual_x))**2)/len(reference_x)
    plt.plot(time,actual_x,'k',label='theta')
    plt.plot(time,reference_x,'k--',label='theta_d')
    plt.title('X MSE=%.2f mm'%(mse_x,))
    plt.legend(loc=0, numpoints=1)
    if save_to_file == 'T' or save_to_file == '':
        plt.savefig(fig_name+"_X.png",
                    format = 'png'
                    )
        plt.clf()
    else:
        plt.show()

    mse_y = 100*np.sum((np.array(reference_y)-np.array(actual_y))**2)/len(reference_y)
    plt.plot(time,actual_y,'k',label='theta')
    plt.plot(time,reference_y,'k--',label='theta_d')
    plt.title('Y MSE=%.2f mm'%(mse_y,))
    plt.legend(loc=0, numpoints=1)
    if save_to_file == 'T' or save_to_file == '':
        plt.savefig(fig_name+"_Y.png",
                    format = 'png'
                    )
        plt.clf()
    else:
        plt.show()

    mse_orientation = 100*np.sum((np.array(reference_orientation)-np.array(actual_orientation))**2)/len(reference_orientation)
    plt.plot(time,actual_orientation,'k',label='theta')
    plt.plot(time,reference_orientation,'k--',label='theta_d')
    plt.title('Orientation MSE=%.2f mm'%(mse_orientation,))
    plt.legend(loc=0, numpoints=1)
    if save_to_file == 'T' or save_to_file == '':
        plt.savefig(fig_name+"_theta.png",
                    format = 'png'
                    )
        plt.clf()
    else:
        plt.show()

    plt.plot(time,velocity_linear,'k',label='velocity_linear')
    plt.plot(time,velocity_angular,'k--',label='velocity_angular')
    plt.title('Velocities')
    plt.legend(loc=0, numpoints=1)
    # plt.ylim([-0.35, 0.35])
    if save_to_file == 'T' or save_to_file == '':
        plt.savefig(fig_name+"_vel.png",
                    format = 'png'
                    )
        plt.clf()
    else:
        plt.show()

if __name__ == "__main__":

    if len(sys.argv)>1:
        fname = sys.argv[1]
        plot_from(fname)
    else:
        print("Get file name as a first argument, please!")
