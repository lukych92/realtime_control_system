# Real-time Control System #
## Version 0.1.0 ##

---

OROCOS/ROS real-time packages for motion planning and trajectory tracking problem of differential drive mobile robot. The package delivery also simulation in Gazebo. 

---
### Architecture ###

![system.png](https://bitbucket.org/repo/5pkdAo/images/438232307-system.png)

---
'Motion planner' nodes use ECSA python package available [here](https://bitbucket.org/lukych92/ecsa ). Please follow the package installation instruction.